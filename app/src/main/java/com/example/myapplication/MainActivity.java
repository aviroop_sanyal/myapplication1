package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button sample_button;
    EditText sample_editText;
    TextView sample_textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sample_button = findViewById(R.id.button);
        sample_editText = findViewById(R.id.editText);

        sample_textView = findViewById(R.id.textView);

        sample_textView.setText("Hello World");
        sample_textView.setTextColor(Color.GREEN);
        sample_textView.setTextSize(15);
    }
}
